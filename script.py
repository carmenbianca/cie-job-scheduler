from cie_job_scheduler.job import OdooJob, Pipeline, run_pipelines


def main():
    pipelines = [
        Pipeline(
            [
                OdooJob("database1", "step1"),
                OdooJob("database1", "step2"),
                OdooJob("database1", "step3"),
                OdooJob("database1", "step4"),
                OdooJob("database1", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database2", "step1"),
                OdooJob("database2", "step2"),
                OdooJob("database2", "step3"),
                OdooJob("database2", "step4"),
                OdooJob("database2", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database3", "step1"),
                OdooJob("database3", "step2"),
                OdooJob("database3", "step3"),
                OdooJob("database3", "step4"),
                OdooJob("database3", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database4", "step1"),
                OdooJob("database4", "step2"),
                OdooJob("database4", "step3"),
                OdooJob("database4", "step4"),
                OdooJob("database4", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database5", "step1"),
                OdooJob("database5", "step2"),
                OdooJob("database5", "step3"),
                OdooJob("database5", "step4"),
                OdooJob("database5", "step5"),
            ]
        ),
    ]
    run_pipelines(pipelines)


if __name__ == "__main__":
    main()
