import random
import sys
import time

if __name__ == "__main__":
    print(sys.argv)
    seconds = random.randint(5, 15)
    print(f"sleeping {seconds} seconds")
    time.sleep(seconds)

    sys.exit(0)
