# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from flask import Blueprint, current_app, render_template

# Blueprint for all HTML endpoints.
html_blueprint = Blueprint("html", __name__)


@html_blueprint.route("/", methods=["GET"])
def overview():
    # TODO: We kind of need access to some sort of data structure here that is
    # aware of all the files written to the artefact directory (that is: json
    # files with metadata + log files). I don't know how to keep these cleanly
    # in sync.
    #
    # Once we have the data structure, we render it into a template.
    pass
