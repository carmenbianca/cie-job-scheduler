# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import atexit

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask

from .job import run_pipelines


def create_and_run_pipelines():
    pipelines = _create_pipelines()
    run_pipelines(pipelines)


def _create_pipelines():
    # TODO: Find databases, create pipelines for each.
    return [
        Pipeline(
            [
                OdooJob("database1", "step1"),
                OdooJob("database1", "step2"),
                OdooJob("database1", "step3"),
                OdooJob("database1", "step4"),
                OdooJob("database1", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database2", "step1"),
                OdooJob("database2", "step2"),
                OdooJob("database2", "step3"),
                OdooJob("database2", "step4"),
                OdooJob("database2", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database3", "step1"),
                OdooJob("database3", "step2"),
                OdooJob("database3", "step3"),
                OdooJob("database3", "step4"),
                OdooJob("database3", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database4", "step1"),
                OdooJob("database4", "step2"),
                OdooJob("database4", "step3"),
                OdooJob("database4", "step4"),
                OdooJob("database4", "step5"),
            ]
        ),
        Pipeline(
            [
                OdooJob("database5", "step1"),
                OdooJob("database5", "step2"),
                OdooJob("database5", "step3"),
                OdooJob("database5", "step4"),
                OdooJob("database5", "step5"),
            ]
        ),
    ]


def create_app():
    # create and configure the app
    app = Flask("cie-job-scheduler")
    app.config.from_object(config)

    app.scheduler = BackgroundScheduler()
    app.scheduler.add_job(
        func=create_and_run_pipelines,
        trigger="cron",
        hour="1",
        minute="0",
    )
    app.scheduler.start()

    atexit.register(lambda: app.scheduler.shutdown())

    return app
