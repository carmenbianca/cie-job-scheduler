# SPDX-FileCopyrightText: 2023 Coop IT Easy SC
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import pathlib
import subprocess
from abc import ABC, abstractmethod
from typing import Final, Optional, Sequence, Tuple, Iterable
from concurrent.futures import FIRST_COMPLETED, ThreadPoolExecutor, wait


# TODO: Temporary, obviously.
OOW_PATH = (pathlib.Path(__file__).resolve() / "../../../oow.py").resolve()

MAX_WORKERS = 5


class Result:
    def __init__(self, success: bool, log: str):
        self.success = success
        self.log = log


class Job(ABC):
    def __init__(self) -> None:
        # FIXME: Let's wrap these behind a Lock maybe.
        self.finished: bool = False
        self.result: Optional[Result] = None

    @abstractmethod
    def run(self) -> Result:
        pass


class OdooJob(Job):
    def __init__(self, database: str, step: str):
        super().__init__()
        self.database = database
        self.step = step

    def run(self) -> Result:
        if self.finished:
            # TODO: Use a better exception type.
            raise Exception("this job is already finished.")
        out = subprocess.run(
            ["python3", "oow.py", self.database, self.step],
            check=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            # Decode output as UTF-8. Who wants to deal with bytes objects?
            text=True,
        )
        self.finished = True
        self.result = Result(out.returncode == 0, out.stdout)
        return self.result


class Pipeline:
    def __init__(self, jobs: Sequence[Job]):
        if not jobs:
            raise ValueError("jobs must not be an empty sequence.")
        # Mark *jobs* immutable.
        self.jobs: Final[Tuple[Job, ...]] = tuple(jobs)

    def run_next_job(self) -> Result:
        job = self.get_next_job()
        if not job:
            # TODO: Change exception type.
            raise Exception("no more jobs remaining")
        result = job.run()
        return result

    def get_next_job(self) -> Optional[Job]:
        for job in self.jobs:
            if not job.finished:
                return job
        return None


def _run_next_job(pipeline: Pipeline) -> Result:
    return pipeline.run_next_job()


def run_pipelines(pipelines: Iterable[Pipeline]) -> None:
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        futures = {
            executor.submit(_run_next_job, pipeline): pipeline for pipeline in pipelines
        }
        futures_done = {}

        while futures:
            done, not_done = wait(futures, return_when=FIRST_COMPLETED)
            for future in done:
                # Move key-value pair to futures_done.
                pipeline = futures.pop(future)
                futures_done[future] = pipeline

                # Add new pipeline job to futures if one exists.
                if pipeline.get_next_job():
                    futures[executor.submit(_run_next_job, pipeline)] = pipeline

                # TODO: remove me
                result = future.result()
                print(result.success, result.log)
                print()

                # FIXME: Let's call/ping a hook here that signals that the job
                # is done, kind of like Qt Signals. An analogous Slot receives
                # the signal, and acts on it in an unrelated thread.
                #
                # For our purposes, the Slot will generate the necessary
                # artefact files for the Flask app.
