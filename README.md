<!--
SPDX-FileCopyrightText: 2023 Coop IT Easy SC

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# cie-job-scheduler

This is a prototype attempt at continuous migration. It's supposed to be a
program/system/interface/thingamabob whose primary purpose is to continuously
(say, once per day) migrate all of CIE's databases to later versions.

The results and logs of these migrations must be saved and displayed.

## Install

```bash
poetry install
```

## Summary

This is a summary of the prototype written on 2023-01-18. It may become quickly
invalid if things change.

### cie_job_scheduler.job

A simple pipeline/job load balancer thingamajig. A Pipeline has several Jobs.
The Jobs are run in sequence, and contain their Result (success status, log).

`run_pipelines` takes a list of Pipelines, and always runs InsertNumberHere Jobs
simultaneously from those Pipelines using a ThreadPoolExecutor. When a Job is
finished, a new Job from the Pipeline (if it's not the last Job) is added to the
queue.

Instead of running against a real version of odoo-openupgrade-wizard, `oow.py`
is used instead to simulate a program that takes a while to run.

Once a Job is finished, it should create a pair of artefacts (e.g., metadata in
json file, log file) and store them in a known directory structure. It does not
do this yet.

`script.py` can be used to run a bunch of Pipelines using `run_pipelines`.

### cie_job_scheduler.app

Creates a Flask app. A thread is automatically created on a daily cronjob to run
a new set of Pipelines.

### cie_job_scheduler.views

Should, but does not yet, contain functions for HTTP endpoints. Once an endpoint
is reached, an HTML template is rendered. The problem is that a data structure
misses for parsing the job artefacts.

From a comment:

```python
# TODO: We kind of need access to some sort of data structure here that is
# aware of all the files written to the artefact directory (that is: json
# files with metadata + log files). I don't know how to keep these cleanly
# in sync.
#
# Once we have the data structure, we render it into a template.
```

## Thoughts

It works, but isn't amazing. It runs the pipelines just fine, but:

- Creating the Pipelines + Jobs (in advance of running the Pipelines) will
  require a spaghetti mess of custom code. I suppose that this is expected.
- The Pipeline/Job objects that are used to run the pipelines have nothing to do
  with the Python data structure that will need to be created to render the
  HTML. Something like an ORM would be nice, but maybe that would also be
  over-engineering.
  - Alternatively, we could split job.py from the Flask program. Just run the
    pipelines on a literal cronjob as a separate process. Then, from that
    process, POST to some URL owned by the Flask program whenever a Job is
    finished, which allows the Flask program to add the job's results to its
    internal data structure.
- Some effort will need to go into making the HTML templates look somewhat
  presentable.
- The Flask server will need to be password-protected. This shouldn't be that
  difficult, hopefully.
